// Get the container element
var btnContainer = document.getElementById("navigation");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("btn");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += "active";
  });
}
$(function() {
  var d = 300;
  $("#navigation a").each(function() {
    var $this = $(this);
    var r = Math.floor(Math.random() * 41) - 20;
    $this.stop().animate(
      {
        marginTop: "-70px"
      },
      (d += 150)
    );
  });
  $("#navigation > li")
    .hover(
      function() {
        var $this = $(this);
        $("a", $this)
          .stop()
          .animate(
            {
              marginTop: "-40px"
            },
            0
          );
      },
      function() {
        var $this = $(this);
        $("a", $this)
          .stop()
          .animate(
            {
              marginTop: "-70px"
            },
            0
          );
      }
    )
    .click(function() {
      var $this = $(this);
      var name = this.className;
      $("#content").animate({ marginTop: -600 }, 300, function() {
        var $this = $(this);
        var r = Math.floor(Math.random() * 41) - 20;
        $("#content div").hide();
        $("#" + name).show();
        $this.animate({ marginTop: -200 }, 0);
      });
    });
});
