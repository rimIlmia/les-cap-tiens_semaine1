$(function () {
    var slickOpts = {
        dots: false,
        infinite: true,
        speed: 700,
        autoplay: true,
        arrows:false
    };
    // Init the slick
    $('.single-item').slick(slickOpts);
    var slickEnabled = true;
   
});